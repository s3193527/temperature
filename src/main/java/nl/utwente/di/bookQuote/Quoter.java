package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {


    public double getBookPrice(String isbn) {
        Map<String, Double> isbnResults = new HashMap<>();
        isbnResults.put("1", 10.0);
        isbnResults.put("2", 45.0);
        isbnResults.put("3", 20.0);
        isbnResults.put("4", 35.0);
        isbnResults.put("5", 50.0);

        return isbnResults.getOrDefault(isbn, 0.0);
    }
}
