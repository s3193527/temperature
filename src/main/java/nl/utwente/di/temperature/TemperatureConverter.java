package nl.utwente.di.temperature;

public class TemperatureConverter {

    public double celsiusToFahrenheit(String celsius) {
        return (Double.valueOf(celsius) * (9.0/5.0)) + 32;
    }
}
